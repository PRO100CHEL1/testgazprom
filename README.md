# Проект по развертыванию OpenResty и Netdata

## Описание проекта

Этот проект настраивает два Docker контейнера: OpenResty версии 1.21.4.3-2 на базе Debian Bullseye и Netdata версии 1.22.1. Контейнер Netdata настроен на прослушивание порта 29999, а OpenResty проксирует запросы на этот порт через порт 8080.

## Установка

### Требования

- Docker
- Docker Compose
- Ansible
- Git

### Шаги установки

1. Клонируйте репозиторий:

    git clone https://gitlab.com/yourusername/yourprojectname.git
    cd yourprojectname
    

2. Запустите Docker Compose:

    docker-compose up -d


3. Используйте Ansible для развертывания:
    
    ansible-playbook deploy.yml


## CI/CD

Проект настроен на использование GitLab CI/CD для автоматического деплоя при каждом коммите в основную ветку. Конфигурация находится в файле `.gitlab-ci.yml`.


### Полезные материалы

1. **GitLab CI/CD**:
   - [Документация GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
   - [GitLab CI/CD Getting Started](https://docs.gitlab.com/ee/ci/quick_start/)

2. **Docker**:
   - [Документация Docker](https://docs.docker.com/)
   - [Docker Compose](https://docs.docker.com/compose/)

3. **Ansible**:
   - [Документация Ansible](https://docs.ansible.com/ansible/latest/index.html)
